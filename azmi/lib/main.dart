import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import './email.dart' as email;
import './music.dart' as music;
import './shopping.dart' as shopping;
import './telepon.dart' as telepon;

void main() {
  runApp(MaterialApp(
    debugShowCheckedModeBanner: false,
    title: "Tampilan Tab Bar",
    home: HalamanPertama(),
  ));
}

class HalamanPertama extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: <Widget>[
          Container(
            child: Column(
              children: <Widget>[
                Image.asset(
                  'assets/18.png',
                  height: 250,
                  width: 500,
                ),
                Text(
                  'GUARD',
                  style: TextStyle(
                      color: Colors.brown,
                      fontSize: 25.0,
                      fontWeight: FontWeight.bold),
                ),
                Text(
                  'THE CORONA VIRUS',
                  style: TextStyle(
                      color: Colors.grey,
                      fontSize: 18.0,
                      fontWeight: FontWeight.bold),
                ),
              ],
            ),
            height: 350,
            width: double.infinity,
            decoration: BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment.topRight,
                end: Alignment.bottomLeft,
                colors: [Colors.white, Colors.lightGreenAccent],
              ),
            ),
          ),
          Center(
            child: RaisedButton(
              child: Text('               Tap Here                  '),
              onPressed: () {
                Navigator.push(
                    context, MaterialPageRoute(builder: (context) => Home()));
              },
            ),
          ),
        ],
      ),
    );
  }
}

// class MyClipper extends CustomClipper<Path>{
//   @override
//   Path getClip(Size size) {
//       var path = Path();
//       path.lineTo(0, size.height - 80);
//       path.quadraticBezierTo(
//         size.width/2, size.height, size.width, size.height - 80);
//         path.lineTo(size.width, 0);
//         path.close();
//       return path;
//     }
// }

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> with SingleTickerProviderStateMixin {
  //create controller untuk tabBar
  TabController controller;

  @override
  void initState() {
    controller = new TabController(vsync: this, length: 4);
    //tambahkan SingleTickerProviderStateMikin pada class _HomeState
    super.initState();
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      //create appBar
      appBar: new AppBar(
        //warna background
        flexibleSpace: Container(
          decoration: BoxDecoration(
              gradient: LinearGradient(
                  begin: Alignment.topLeft,
                  end: Alignment.bottomRight,
                  colors: <Color>[
                Colors.lightGreen,
                Colors.lightGreenAccent,
                Colors.white,
              ])),
        ),
        //judul
        //title: new Text("Tampilan Home "),
        //bottom
        bottom: new TabBar(
          controller: controller,
          //source code lanjutan
          tabs: <Widget>[
            new Tab(
              icon: new Icon(Icons.home),
              //text: "Home",
            ),
            new Tab(
              icon: new Icon(Icons.queue_music),
              //text: "Music",
            ),
            new Tab(
              icon: new Icon(Icons.shopping_cart),
              //text: "Shop",
            ),
            new Tab(
              icon: new Icon(Icons.phone_android),
              //text: "Phone",
            ),
          ],
        ),
      ),
      body: new TabBarView(
        controller: controller,
        children: <Widget>[
          new email.Email(),
          new music.Music(),
          new shopping.Shopping(),
          new telepon.Telepon()
        ],
      ),
    );
  }
}
