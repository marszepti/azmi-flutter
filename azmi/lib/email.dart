import 'package:flutter/material.dart';

class Email extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Home',
      home: Scaffold(
          //backgroundColor: Colors.yellowAccent,
          body: ListView(children: <Widget>[
        Container(
          child: Column(
            children: <Widget>[
              Image.asset(
                'assets/banco2.png',
              ),
            ],
          ),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            Container(
              child: Card(
                child: Column(
                  children: [
                    Image.asset(
                      'assets/sad.png',
                      height: 70,
                      width: 70,
                    ),
                    Text(
                      'Positif',
                      style: TextStyle(
                          color: Colors.brown,
                          fontSize: 17.0,
                          fontWeight: FontWeight.bold),
                    ),
                  ],
                ),
                elevation: 15,
              ),
              height: 200,
              width: 170,
              padding: EdgeInsets.all(10.0),
              margin: EdgeInsets.all(5.0),
              //decoration: BoxDecoration(color: Colors.lightBlueAccent),
            ),
            Container(
              child: Card(
                child: Column(
                  children: [
                    Image.asset(
                      'assets/happy.png',
                      height: 70,
                      width: 70,
                    ),
                    Text(
                      'Sembuh',
                      style: TextStyle(
                          color: Colors.brown,
                          fontSize: 17.0,
                          fontWeight: FontWeight.bold),
                    ),
                  ],
                ),
                elevation: 15,
              ),
              height: 200,
              width: 170,
              padding: EdgeInsets.all(10.0),
              margin: EdgeInsets.all(5.0),
              //decoration: BoxDecoration(color: Colors.lightBlueAccent),
            ),
          ],
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            Container(
              child: Card(
                child: Column(
                  children: [
                    Image.asset(
                      'assets/sosad.png',
                      height: 70,
                      width: 70,
                    ),
                    Text(
                      'Meninggal',
                      style: TextStyle(
                          color: Colors.brown,
                          fontSize: 17.0,
                          fontWeight: FontWeight.bold),
                    ),
                  ],
                ),
                elevation: 15,
              ),
              height: 200,
              width: 170,
              padding: EdgeInsets.all(10.0),
              margin: EdgeInsets.all(5.0),
              //decoration: BoxDecoration(color: Colors.lightBlueAccent),
            ),
            Container(
              child: Card(
                child: Column(
                  children: [
                    Image.asset(
                      'assets/flag.png',
                      height: 100,
                      width: 100,
                    ),
                    Text(
                      'Indonesia',
                      style: TextStyle(
                          color: Colors.brown,
                          fontSize: 17.0,
                          fontWeight: FontWeight.bold),
                    ),
                  ],
                ),
                elevation: 15,
              ),
              height: 200,
              width: 170,
              padding: EdgeInsets.all(10.0),
              margin: EdgeInsets.all(5.0),
              //decoration: BoxDecoration(color: Colors.lightBlueAccent),
            ),
          ],
        ),
      ])),
    );
  }
}
