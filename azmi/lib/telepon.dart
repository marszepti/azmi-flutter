import 'package:flutter/material.dart';

class Telepon extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.lightBlueAccent,
      body: ListView(children: <Widget>[
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            Image.asset(
              'assets/1.jpg',
              height: 100,
              width: 100,
            ),
            Image.asset(
              'assets/2.jpg',
              height: 100,
              width: 100,
            ),
          ],
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            Image.asset(
              'assets/3.jpg',
              height: 100,
              width: 100,
            ),
            Image.asset(
              'assets/4.jpg',
              height: 100,
              width: 100,
            ),
          ],
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            Image.asset(
              'assets/5.jpg',
              height: 100,
              width: 100,
            ),
            Image.asset(
              'assets/6.jpg',
              height: 100,
              width: 100,
            ),
          ],
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            Image.asset(
              'assets/7.jpg',
              height: 100,
              width: 100,
            ),
            Image.asset(
              'assets/8.jpg',
              height: 100,
              width: 100,
            ),
          ],
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            Image.asset(
              'assets/9.jpg',
              height: 100,
              width: 100,
            ),
            Image.asset(
              'assets/10.jpg',
              height: 100,
              width: 100,
            ),
          ],
        ),
      ]),
    );
  }
}
