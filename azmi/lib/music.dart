import 'package:flutter/material.dart';

void main() {
  runApp(new MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Music(),
    );
  }
}

class Music extends StatelessWidget {
  final List music = [
    "BTS - Fire",
    "EXO - Kokobop",
    "Rihanna - Diamond",
    "Cardi B - I like it",
    "Bruno Mars - Lazy",
    "Zayn - Pillow Talk",
    "Charlie Puth - Attention",
    "Rizky Febian - Cuek",
    "St12 - Saat Terakhir",
    "Shawn Mendes - Imagination",
    "Blackpink - So hot",
    "Red Velvet - Bad Boy"
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView.builder(
        itemBuilder: (context, index) {
          return Card(
            child: Padding(
              padding: const EdgeInsets.all(15.0),
              child: Text(music[index], style: TextStyle(fontSize: 20)),
            ),
          );
        },
        itemCount: music.length,
      ),
    );
  }
}
