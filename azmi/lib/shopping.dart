import 'package:flutter/material.dart';

class Shopping extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'First App',
      home: Scaffold(
          backgroundColor: Colors.purpleAccent,
          body: ListView(children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Container(
                  child: Column(
                    children: <Widget>[
                      Image.asset(
                        'assets/nacific.jpg',
                        height: 100,
                        width: 100,
                      ),
                      Text(
                        '',
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 13.0,
                            fontWeight: FontWeight.bold),
                      ),
                      Text(
                        'Nacific',
                        style: TextStyle(
                            color: Colors.black87,
                            fontSize: 13.0,
                            fontWeight: FontWeight.bold),
                      ),
                      Text(
                        'Rp.250.000',
                        style: TextStyle(
                            color: Colors.orange,
                            fontSize: 10.0,
                            fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
                  height: 180,
                  width: 160,
                  padding: EdgeInsets.all(15.0),
                  margin: EdgeInsets.all(9.0),
                  decoration: BoxDecoration(color: Colors.yellowAccent),
                ),
                Container(
                  child: Column(
                    children: <Widget>[
                      Image.asset(
                        'assets/nacific2.jpg',
                        height: 100,
                        width: 100,
                      ),
                      Text(
                        '',
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 13.0,
                            fontWeight: FontWeight.bold),
                      ),
                      Text(
                        'Nacific',
                        style: TextStyle(
                            color: Colors.black87,
                            fontSize: 13.0,
                            fontWeight: FontWeight.bold),
                      ),
                      Text(
                        'Rp.260.000',
                        style: TextStyle(
                            color: Colors.orange,
                            fontSize: 10.0,
                            fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
                  height: 180,
                  width: 160,
                  padding: EdgeInsets.all(15.0),
                  margin: EdgeInsets.all(9.0),
                  decoration: BoxDecoration(color: Colors.yellowAccent),
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Container(
                  child: Column(
                    children: <Widget>[
                      Image.asset(
                        'assets/makeover1.jpg',
                        height: 100,
                        width: 100,
                      ),
                      Text(
                        '',
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 13.0,
                            fontWeight: FontWeight.bold),
                      ),
                      Text(
                        'Makeover',
                        style: TextStyle(
                            color: Colors.black87,
                            fontSize: 13.0,
                            fontWeight: FontWeight.bold),
                      ),
                      Text(
                        'Rp.300.000',
                        style: TextStyle(
                            color: Colors.orange,
                            fontSize: 10.0,
                            fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
                  height: 180,
                  width: 160,
                  padding: EdgeInsets.all(15.0),
                  margin: EdgeInsets.all(9.0),
                  decoration: BoxDecoration(color: Colors.yellowAccent),
                ),
                Container(
                  child: Column(
                    children: <Widget>[
                      Image.asset(
                        'assets/makeover2.jpg',
                        height: 100,
                        width: 100,
                      ),
                      Text(
                        '',
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 13.0,
                            fontWeight: FontWeight.bold),
                      ),
                      Text(
                        'Makeover',
                        style: TextStyle(
                            color: Colors.black87,
                            fontSize: 13.0,
                            fontWeight: FontWeight.bold),
                      ),
                      Text(
                        'Rp.160.000',
                        style: TextStyle(
                            color: Colors.orange,
                            fontSize: 10.0,
                            fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
                  height: 180,
                  width: 160,
                  padding: EdgeInsets.all(15.0),
                  margin: EdgeInsets.all(9.0),
                  decoration: BoxDecoration(color: Colors.yellowAccent),
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Container(
                  child: Column(
                    children: <Widget>[
                      Image.asset(
                        'assets/wardah1.jpg',
                        height: 100,
                        width: 100,
                      ),
                      Text(
                        '',
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 13.0,
                            fontWeight: FontWeight.bold),
                      ),
                      Text(
                        'Wardah',
                        style: TextStyle(
                            color: Colors.black87,
                            fontSize: 13.0,
                            fontWeight: FontWeight.bold),
                      ),
                      Text(
                        'Rp.120.000',
                        style: TextStyle(
                            color: Colors.orange,
                            fontSize: 10.0,
                            fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
                  height: 180,
                  width: 160,
                  padding: EdgeInsets.all(15.0),
                  margin: EdgeInsets.all(9.0),
                  decoration: BoxDecoration(color: Colors.yellowAccent),
                ),
                Container(
                  child: Column(
                    children: <Widget>[
                      Image.asset(
                        'assets/wardah2.jpg',
                        height: 100,
                        width: 100,
                      ),
                      Text(
                        '',
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 13.0,
                            fontWeight: FontWeight.bold),
                      ),
                      Text(
                        'Wardah',
                        style: TextStyle(
                            color: Colors.black87,
                            fontSize: 13.0,
                            fontWeight: FontWeight.bold),
                      ),
                      Text(
                        'Rp.200.000',
                        style: TextStyle(
                            color: Colors.orange,
                            fontSize: 10.0,
                            fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
                  height: 180,
                  width: 160,
                  padding: EdgeInsets.all(15.0),
                  margin: EdgeInsets.all(9.0),
                  decoration: BoxDecoration(color: Colors.yellowAccent),
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Container(
                  child: Column(
                    children: <Widget>[
                      Image.asset(
                        'assets/safi1.JPG',
                        height: 100,
                        width: 100,
                      ),
                      Text(
                        '',
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 13.0,
                            fontWeight: FontWeight.bold),
                      ),
                      Text(
                        'Safi',
                        style: TextStyle(
                            color: Colors.black87,
                            fontSize: 13.0,
                            fontWeight: FontWeight.bold),
                      ),
                      Text(
                        'Rp.250.000',
                        style: TextStyle(
                            color: Colors.orange,
                            fontSize: 10.0,
                            fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
                  height: 180,
                  width: 160,
                  padding: EdgeInsets.all(15.0),
                  margin: EdgeInsets.all(9.0),
                  decoration: BoxDecoration(color: Colors.yellowAccent),
                ),
                Container(
                  child: Column(
                    children: <Widget>[
                      Image.asset(
                        'assets/safi2.jpg',
                        height: 100,
                        width: 100,
                      ),
                      Text(
                        '',
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 13.0,
                            fontWeight: FontWeight.bold),
                      ),
                      Text(
                        'Safi',
                        style: TextStyle(
                            color: Colors.black87,
                            fontSize: 13.0,
                            fontWeight: FontWeight.bold),
                      ),
                      Text(
                        'Rp.260.000',
                        style: TextStyle(
                            color: Colors.orange,
                            fontSize: 10.0,
                            fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
                  height: 180,
                  width: 160,
                  padding: EdgeInsets.all(15.0),
                  margin: EdgeInsets.all(9.0),
                  decoration: BoxDecoration(color: Colors.yellowAccent),
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Container(
                  child: Column(
                    children: <Widget>[
                      Image.asset(
                        'assets/emina1.jpg',
                        height: 100,
                        width: 100,
                      ),
                      Text(
                        '',
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 13.0,
                            fontWeight: FontWeight.bold),
                      ),
                      Text(
                        'Emina',
                        style: TextStyle(
                            color: Colors.black87,
                            fontSize: 13.0,
                            fontWeight: FontWeight.bold),
                      ),
                      Text(
                        'Rp.60.000',
                        style: TextStyle(
                            color: Colors.orange,
                            fontSize: 10.0,
                            fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
                  height: 180,
                  width: 160,
                  padding: EdgeInsets.all(15.0),
                  margin: EdgeInsets.all(9.0),
                  decoration: BoxDecoration(color: Colors.yellowAccent),
                ),
                Container(
                  child: Column(
                    children: <Widget>[
                      Image.asset(
                        'assets/emina2.jpg',
                        height: 100,
                        width: 100,
                      ),
                      Text(
                        '',
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 13.0,
                            fontWeight: FontWeight.bold),
                      ),
                      Text(
                        'Emina',
                        style: TextStyle(
                            color: Colors.black87,
                            fontSize: 13.0,
                            fontWeight: FontWeight.bold),
                      ),
                      Text(
                        'Rp.80.000',
                        style: TextStyle(
                            color: Colors.orange,
                            fontSize: 10.0,
                            fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
                  height: 180,
                  width: 160,
                  padding: EdgeInsets.all(15.0),
                  margin: EdgeInsets.all(9.0),
                  decoration: BoxDecoration(color: Colors.yellowAccent),
                ),
              ],
            ),
          ])),
    );
  }
}
